package com.example.demo.controller;

import com.example.demo.entiy.GoodsSort;
import com.example.demo.service.GoodsSortService;
import com.example.demo.entiy.ReturnMessage;
import com.example.demo.entiy.ReturnSelectGoodsOrt;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/tgoodsort")
public class GoodsSortController {
	@Autowired
	GoodsSortService goodsSortService;

	@ResponseBody
	@RequestMapping(value = "/allgoodsort", method = RequestMethod.GET)
	public List<GoodsSort> getData() {
		List<GoodsSort> list = goodsSortService.getAll();
		System.out.print(list);
		return list;
	}
	
	@RequestMapping(value = "/getSelectGoodsOrt", method = RequestMethod.POST)
	public ReturnSelectGoodsOrt getSelectGoodsSorts(@RequestBody Map<String, Object> map) {
		String message = "API加载失败";
		boolean success;
		String gOODS_SORT_CD = (String) map.get("gOODS_SORT_CD");
		String mANUF_DDLV_YN = (String) map.get("mANUF_DDLV_YN");
		String sORT_NM = (String) map.get("sORT_NM");
		System.out.println(gOODS_SORT_CD+"/"+mANUF_DDLV_YN+"/"+sORT_NM);
		ReturnSelectGoodsOrt returnData = new ReturnSelectGoodsOrt();
		List<GoodsSort> list = null;
		try {
			GoodsSort goodsSort = new GoodsSort();
			goodsSort.setGOODS_SORT_CD(gOODS_SORT_CD);
			goodsSort.setMANUF_DDLV_YN(mANUF_DDLV_YN);
			goodsSort.setSORT_NM(sORT_NM);
			list = goodsSortService.getSelect(goodsSort);
			message = "查询完成";
			success = true;
		} catch(Exception e) {
			message = "失败  [ERROR:" + e.getMessage() + "]";
			System.out.println(message);
			success = false;
		}
		System.out.println(list);

		returnData.setMessage(message);
		returnData.setSuccess(success);
		returnData.setDataList(list);
		
		return returnData;
	}

	@RequestMapping(value = "/insertgoodsort", method = RequestMethod.POST)
	public ReturnMessage insertGoodsOrtData(@RequestBody Map<String, Object> map) {
		String message = "";
		boolean success;
		String gOODS_SORT_CD = (String) map.get("gOODS_SORT_CD");
		String mANUF_DDLV_YN = (String) map.get("mANUF_DDLV_YN");
		String sORT_NM = (String) map.get("sORT_NM");
		System.out.println(gOODS_SORT_CD+"/"+mANUF_DDLV_YN+"/"+sORT_NM);
		ReturnMessage returnMessage = new ReturnMessage();
		if(map.get("gOODS_SORT_CD")==null) {
			message = "商品分類コードを選択してください。";
			success = false;
		} else {
			try {
				GoodsSort goodsSort = new GoodsSort();
				goodsSort.setGOODS_SORT_CD(gOODS_SORT_CD);
				goodsSort.setMANUF_DDLV_YN(mANUF_DDLV_YN);
				goodsSort.setSORT_NM(sORT_NM);
				goodsSortService.insertGoodsSorts(goodsSort);
				message = map.get("gOODS_SORT_CD") + "新規完成";
				success = true;
			} catch(Exception e) {
				message = " 新規失败  [ERROR:" + e.getMessage() + "]";
				if(e.getMessage().indexOf("java.sql.SQLIntegrityConstraintViolationException:") != -1) {
					message = "新規したい商品部門はすでに存在しています。";
				}
				success = false;
			}
		}
		System.out.println(gOODS_SORT_CD+"/"+mANUF_DDLV_YN+"/"+sORT_NM);

		returnMessage.setMessage(message);
		returnMessage.setSuccess(success);
		
		return returnMessage;
	}
	
	@RequestMapping(value = "/upgoodsort", method = RequestMethod.POST)
	public ReturnMessage upGoodsSort(@RequestBody Map<String, Object> map) {
		String message = "";
		boolean success;
		String gOODS_SORT_CD = (String) map.get("gOODS_SORT_CD");
		String mANUF_DDLV_YN = (String) map.get("mANUF_DDLV_YN");
		String sORT_NM = (String) map.get("sORT_NM");
		ReturnMessage returnMessage = new ReturnMessage();
		if(gOODS_SORT_CD.equals("")) {
			message = "商品分類コード不能为空";
			success = false;
		} else {
			try {
				GoodsSort goodsSort = new GoodsSort();
				goodsSort.setGOODS_SORT_CD(gOODS_SORT_CD);
				goodsSort.setMANUF_DDLV_YN(mANUF_DDLV_YN);
				goodsSort.setSORT_NM(sORT_NM);
				success = goodsSortService.upGoodsSort(goodsSort);
				message = map.get("gOODS_SORT_CD") + "修正完成";
				if(success==false) {
					message = "更新データがありません";
				}
			} catch(Exception e) {
				message = " 修正失败  [ERROR:" + e.getMessage() + "]";
				success = false;
			}
		}
		returnMessage.setMessage(message);
		returnMessage.setSuccess(success);
		
		return returnMessage;
	}
	
	@RequestMapping(value = "/deleteGoodsOrt", method = RequestMethod.POST)
	public ReturnMessage deleteGoodsSort(@RequestBody Map<String, Object> map) {
		String message = "";
		boolean success;
		String gOODS_SORT_CD = (String) map.get("gOODS_SORT_CD");
		String mANUF_DDLV_YN = (String) map.get("mANUF_DDLV_YN");
		String sORT_NM = (String) map.get("sORT_NM");
		ReturnMessage returnMessage = new ReturnMessage();
		if(gOODS_SORT_CD.equals("")) {
			message = "商品分類コードを選択してください。";
			success = false;
		} else {
			try {
				GoodsSort goodsSort = new GoodsSort();
				goodsSort.setGOODS_SORT_CD(gOODS_SORT_CD);
				goodsSort.setMANUF_DDLV_YN(mANUF_DDLV_YN);
				goodsSort.setSORT_NM(sORT_NM);
				goodsSortService.deleteGoodsSort(goodsSort);
				message = map.get("gOODS_SORT_CD") + "削除完成";
				System.out.println(message);
				success = true;
			} catch(Exception e) {
				message = " 削除失败  [ERROR:" + e.getMessage() + "]";
				System.out.println(message);
				success = false;
			}
		}
		returnMessage.setMessage(message);
		returnMessage.setSuccess(success);
		
		return returnMessage;
	}
}
