package com.example.demo.controller;

import com.example.demo.entiy.Goods;
import com.example.demo.entiy.GoodsSort;
import com.example.demo.service.GoodsService;
import com.example.demo.entiy.ReturnMessage;
import com.example.demo.entiy.ReturnSelectGoods;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/tgoods")
public class GoodsController {
	@Autowired
	GoodsService goodsService;

	@ResponseBody
	@RequestMapping(value = "/getSelectGoods", method = RequestMethod.POST)
	public ReturnSelectGoods getSelectGoods(@RequestBody Map<String, Object> map) {
		String message = "API加载失败";
		boolean success;
		String gOODS_NO = (String) map.get("gOODS_NO");
		String gOODS_NM = (String) map.get("gOODS_NM");
		String gOODS_SORT_CD = (String) map.get("gOODS_SORT_CD");
		String mANUF_DDLV_YN = (String) map.get("mANUF_DDLV_YN");
		String sORT_NM = (String) map.get("sORT_NM");
		System.out.println(gOODS_SORT_CD+"/"+mANUF_DDLV_YN+"/"+sORT_NM);
		ReturnSelectGoods returnData = new ReturnSelectGoods();
		List<Goods> list = null;
		try {
			Goods goods = new Goods();
			goods.setGOODS_NO(gOODS_NO);
			goods.setGOODS_NM(gOODS_NM);
			goods.setGOODS_SORT_CD(gOODS_SORT_CD);
			goods.setMANUF_DDLV_YN(mANUF_DDLV_YN);
			goods.setSORT_NM(sORT_NM);
			list = goodsService.getSelectGoods(goods);
			message = "查询完成";
			success = true;
		} catch(Exception e) {
			message = "失败  [ERROR:" + e.getMessage() + "]";
			System.out.println(message);
			success = false;
		}
		System.out.println(list);

		returnData.setMessage(message);
		returnData.setSuccess(success);
		returnData.setDataList(list);
		
		return returnData;
	}
	
	@RequestMapping(value = "/insertgoods", method = RequestMethod.POST)
	public ReturnMessage insertGoodsData(@RequestBody Map<String, Object> map) {
		String message = "";
		boolean success;
		String gOODS_NO = (String) map.get("gOODS_NO");
		String gOODS_NM = (String) map.get("gOODS_NM");
		String gOODS_SORT_CD = (String) map.get("gOODS_SORT_CD");
		String mANUF_DDLV_YN = (String) map.get("mANUF_DDLV_YN");
		String sORT_NM = (String) map.get("sORT_NM");
		System.out.println(gOODS_NO+"/"+gOODS_NM+"/"+gOODS_SORT_CD+"/"+mANUF_DDLV_YN+"/"+sORT_NM);
		ReturnMessage returnMessage = new ReturnMessage();
		if(gOODS_NO==null) {
			message = "商品番号を入力してください。";
			success = false;
		}else if(gOODS_SORT_CD==null) {
			message = "商品分類コードを選択してください。";
			success = false;
		}else {
			if(mANUF_DDLV_YN==null) {
				mANUF_DDLV_YN = "0";
			}
			try {
				Goods goods = new Goods();
				goods.setGOODS_NO(gOODS_NO);
				goods.setGOODS_NM(gOODS_NM);
				goods.setGOODS_SORT_CD(gOODS_SORT_CD);
				goods.setMANUF_DDLV_YN(mANUF_DDLV_YN);
				goods.setSORT_NM(sORT_NM);
				goodsService.insertGoods(goods);
				message = map.get("gOODS_NO") + "新規完成";
				success = true;
			} catch(Exception e) {
				message = " 新規失败  [ERROR:" + e.getMessage() + "]";
				if(e.getMessage().indexOf("java.sql.SQLIntegrityConstraintViolationException:") != -1) {
					message = "新規したい商品部門はすでに存在しています。";
				}
				success = false;
			}
		}
		System.out.println(gOODS_NO+"/"+gOODS_NM+"/"+gOODS_SORT_CD+"/"+mANUF_DDLV_YN+"/"+sORT_NM);

		returnMessage.setMessage(message);
		returnMessage.setSuccess(success);
		
		return returnMessage;
	}
	
	@RequestMapping(value = "/upgoods", method = RequestMethod.POST)
	public ReturnMessage upGoods(@RequestBody Map<String, Object> map) {
		String message = "";
		boolean success;
		String gOODS_NO = (String) map.get("gOODS_NO");
		String gOODS_NM = (String) map.get("gOODS_NM");
		String gOODS_SORT_CD = (String) map.get("gOODS_SORT_CD");
		String mANUF_DDLV_YN = (String) map.get("mANUF_DDLV_YN");
		String sORT_NM = (String) map.get("sORT_NM");
		System.out.println(gOODS_NO+"/"+gOODS_NM+"/"+gOODS_SORT_CD+"/"+mANUF_DDLV_YN+"/"+sORT_NM);
		ReturnMessage returnMessage = new ReturnMessage();
		if(gOODS_NO==null) {
			message = "商品番号を入力してください。";
			success = false;
		}else if(gOODS_SORT_CD==null) {
			message = "商品分類コードを選択してください。";
			success = false;
		}else {
			if(mANUF_DDLV_YN==null) {
				mANUF_DDLV_YN = "0";
			}
			try {
				Goods goods = new Goods();
				goods.setGOODS_NO(gOODS_NO);
				goods.setGOODS_NM(gOODS_NM);
				goods.setGOODS_SORT_CD(gOODS_SORT_CD);
				goods.setMANUF_DDLV_YN(mANUF_DDLV_YN);
				goods.setSORT_NM(sORT_NM);
				success = goodsService.upGoods(goods);
				message = map.get("gOODS_NO") + "更新完成";
				success = true;
				if(success==false) {
					message = "更新データがありません";
				}
			} catch(Exception e) {
				message = " 修正失败  [ERROR:" + e.getMessage() + "]";
				success = false;
			}
		}
		System.out.println(gOODS_NO+"/"+gOODS_NM+"/"+gOODS_SORT_CD+"/"+mANUF_DDLV_YN+"/"+sORT_NM);

		returnMessage.setMessage(message);
		returnMessage.setSuccess(success);
		
		return returnMessage;
	}
	
	@RequestMapping(value = "/deleteGoods", method = RequestMethod.POST)
	public ReturnMessage deleteGoods(@RequestBody Map<String, Object> map) {
		String message = "";
		boolean success;
		String gOODS_NO = (String) map.get("gOODS_NO");
		String mANUF_DDLV_YN = (String) map.get("mANUF_DDLV_YN");
		System.out.println(gOODS_NO+":删除完成");
		ReturnMessage returnMessage = new ReturnMessage();
		if(gOODS_NO==null) {
			message = "商品番号を入力してください。";
			success = false;
		}else {
			if(mANUF_DDLV_YN==null) {
				mANUF_DDLV_YN = "0";
			}
			try {
				Goods goods = new Goods();
				goods.setGOODS_NO(gOODS_NO);
				goodsService.deleteGoods(goods);
				message = map.get("gOODS_NO") + "削除完成";
				System.out.println(message);
				success = true;
			} catch(Exception e) {
				message = " 削除失败  [ERROR:" + e.getMessage() + "]";
				System.out.println(message);
				success = false;
			}
		}
		returnMessage.setMessage(message);
		returnMessage.setSuccess(success);
		
		return returnMessage;
	}
}