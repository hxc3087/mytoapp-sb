package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@CrossOrigin
public class HelloController {
 
    @RequestMapping(value = "/helloreact", method = RequestMethod.GET)
    @ResponseBody
    public String queryUserList(){
        String s = "[{" + "\"name\":\"奥利给xxxxxxxx\"" + "}]";
        return s;
    }
}