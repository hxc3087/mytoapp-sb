package com.example.demo.entiy;

public class GoodsSort {

    private String GOODS_SORT_CD;
    private String SORT_NM;
    private String MANUF_DDLV_YN;
	public String getGOODS_SORT_CD() {
		return GOODS_SORT_CD;
	}
	public void setGOODS_SORT_CD(String gOODS_SORT_CD) {
		GOODS_SORT_CD = gOODS_SORT_CD;
	}
	public String getSORT_NM() {
		return SORT_NM;
	}
	public void setSORT_NM(String sORT_NM) {
		SORT_NM = sORT_NM;
	}
	public String getMANUF_DDLV_YN() {
		return MANUF_DDLV_YN;
	}
	public void setMANUF_DDLV_YN(String mANUF_DDLV_YN) {
		MANUF_DDLV_YN = mANUF_DDLV_YN;
	}
	@Override
	public String toString() {
		return "GoodsSort [GOODS_SORT_CD=" + GOODS_SORT_CD + ", SORT_NM=" + SORT_NM + ", MANUF_DDLV_YN=" + MANUF_DDLV_YN
				+ "]";
	}

    
}
