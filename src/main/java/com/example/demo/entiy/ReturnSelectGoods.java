package com.example.demo.entiy;

import java.util.List;

public class ReturnSelectGoods {
	private boolean success;
	
	private String message;
	
	private List<Goods> dataList;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<Goods> getDataList() {
		return dataList;
	}

	public void setDataList(List<Goods> dataList) {
		this.dataList = dataList;
	}

	@Override
	public String toString() {
		return "ReturnSelectGoodsOrt [success=" + success + ", message=" + message + ", dataList=" + dataList + "]";
	}
	
	
}
