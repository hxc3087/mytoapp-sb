package com.example.demo.entiy;

import java.util.List;

public class ReturnSelectGoodsOrt {
	private boolean success;
	
	private String message;
	
	private List<GoodsSort> dataList;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<GoodsSort> getDataList() {
		return dataList;
	}

	public void setDataList(List<GoodsSort> dataList) {
		this.dataList = dataList;
	}

	@Override
	public String toString() {
		return "ReturnSelectGoodsOrt [success=" + success + ", message=" + message + ", dataList=" + dataList + "]";
	}
	
	
}
