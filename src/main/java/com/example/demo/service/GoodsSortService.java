package com.example.demo.service;

import com.example.demo.dao.GoodsSortDao;
import com.example.demo.entiy.GoodsSort;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GoodsSortService {
	@Autowired
	private GoodsSortDao goodsSortDao;
    public List<GoodsSort> getAll(){
        return goodsSortDao.getAll();
    }
    
    public List<GoodsSort> getSelect(GoodsSort goodsSort) {
    	return goodsSortDao.getSelect(goodsSort);
    }
    
    public void insertGoodsSorts(GoodsSort goodsSort) {
    	goodsSortDao.insertGoodsSorts(goodsSort);
    }
    
    public boolean upGoodsSort(GoodsSort goodsSort) {
    	return goodsSortDao.upGoodsSort(goodsSort);
    }
    
    public void deleteGoodsSort(GoodsSort goodsSort) {
    	goodsSortDao.deleteGoodsSort(goodsSort);
    }
}
