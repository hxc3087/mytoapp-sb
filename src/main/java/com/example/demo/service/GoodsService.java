package com.example.demo.service;

import com.example.demo.dao.GoodsDao;
import com.example.demo.entiy.Goods;
import com.example.demo.entiy.GoodsSort;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GoodsService {
	@Autowired
	private GoodsDao goodsDao;

    public List<Goods> getSelectGoods(Goods goods) {
    	return goodsDao.getSelectGoods(goods);
    }

    public void insertGoods(Goods goods) {
    	goodsDao.insertGoods(goods);
    }
    
    public boolean upGoods(Goods goods) {
    	return goodsDao.upGoods(goods);
    }
    
    public void deleteGoods(Goods goods) {
    	goodsDao.deleteGoods(goods);
    }
}
