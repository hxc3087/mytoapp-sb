package com.example.demo;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Service
@Controller
@CrossOrigin
public class MysqlData2 {
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
    @RequestMapping(value = "/mysql2", method = RequestMethod.GET)
    @ResponseBody
	public List test() {
		String sql = "select * from TGOODS tg LEFT JOIN TGOODSORT tgo ON tg.GOODS_SORT_CD = tgo.GOODS_SORT_CD WHERE tg.GOODS_NO = '1111' ";
		List mapList = (List) jdbcTemplate.queryForList(sql);
		System.out.print(mapList);
		return mapList;
	}
}