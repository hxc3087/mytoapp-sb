package com.example.demo.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.entiy.Goods;
import com.example.demo.entiy.GoodsSort;

@Mapper
public interface GoodsDao {
	@Autowired
	
	List<Goods> getSelectGoods(Goods goods);
	
	void insertGoods(Goods goods);
	
	boolean upGoods(Goods goods);
	
	void deleteGoods(Goods goods);
}
