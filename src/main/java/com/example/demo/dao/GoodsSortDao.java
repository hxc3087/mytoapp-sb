package com.example.demo.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.entiy.GoodsSort;

@Mapper
public interface GoodsSortDao {
	@Autowired
	// 查询所有
	List<GoodsSort> getAll();
	
	List<GoodsSort> getSelect(GoodsSort goodsSort);
	
	void insertGoodsSorts(GoodsSort goodsSort);
	
	boolean upGoodsSort(GoodsSort goodsSort);
	
	void deleteGoodsSort(GoodsSort goodsSort);
}
